<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}

$author = rudr_get_comment_author_data();

$comment_author = 'Name';
$comment_email = 'Email';
$comment_cookies_1 = ' You accept the ';
$comment_cookies_2 = 'Privacy Policy';


if( isset( $author['name'] ) ): 
    $cookies_name = $author['name']; 
else:
    $cookies_name = ""; 
endif;

if( isset( $author['email'] ) ): 
    $cookies_email = $author['email']; 
else:
    $cookies_email = ""; 
endif;

$default_rating_field = '<p class="comment-form-rating"><label for="review-rating">Rating</label> <input id="review-rating" name="review-rating" type="range" value="3" step="1" min="0" max="5" required /></p>';
$default_review_summary_field = '<p class="comment-form-review-summary"><label for="review-review-summary">Review summary</label> <input id="review-review-summary" name="review-review-summary" type="text" value="" size="30" maxlength="245" required /></p>';
$default_author_field = '<p class="comment-form-author"><label for="review-author">Name</label> <input id="review-author" name="author" type="text" value="' . $cookies_name . '" size="30" maxlength="245" required /></p>';
$default_email_field = '<p class="comment-form-email"><label for="review-email">Email</label> <input id="review-email" name="email" type="email" value="' . $cookies_email . '" size="30" maxlength="100" required /></p>';
$default_cookies_field = '<p class="comment-form-cookies-consent"><input id="review-wp-comment-cookies-consent" name="review-wp-comment-cookies-consent" type="checkbox" required /> <label for="review-wp-comment-cookies-consent">' . $comment_cookies_1 . '<a href="' . get_privacy_policy_url() . '">' . $comment_cookies_2 . '</a></label></p>';
//$default_comment_field = '<p class="comment-form-comment"><label for="review-comment">Your review</label> <textarea id="review-comment" name="comment" cols="45" rows="4" maxlength="500" required="required"></textarea></p>';

$args = array(
    'fields' => array(
        'author' => $default_author_field,
        'email' => $default_email_field,
        'cookies' => $default_cookies_field
    ),
    'label_submit' => 'Order',
    'comment_field' => '',
    'comment_notes_before' => '',
    'id_form' => 'review-form',
    'class_container' => 'review-comment-respond',
    'title_reply' => 'Shipping Details',
    'name_submit' => 'review-submit',
    'id_submit' => 'review-submit',
);
comment_form($args);
?>