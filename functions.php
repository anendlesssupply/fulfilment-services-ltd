<?php
/**
 * Fulfilment Services Ltd functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Fulfilment_Services_Ltd
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'fsl_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function fsl_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Fulfilment Services Ltd, use a find and replace
		 * to change 'fsl' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'fsl', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'align-wide' );

		add_theme_support( 'editor-font-sizes', array(
			array(
			  'name'      => __( 'Small', 'fsl' ),
			  'shortName' => __( 'S', 'fsl' ),
			  'size'      => 13,
			  'slug'      => 'small'
			),
			array(
			  'name'      => __( 'Large', 'fsl' ),
			  'shortName' => __( 'L', 'fsl' ),
			  'size'      => 24,
			  'slug'      => 'large'
			)
		  ) );
	  
		  add_theme_support( 'editor-color-palette', array(
			array(
			  'name'  => 'Black',
			  'slug'  => 'black',
			  'color'	=> '#000000',
			),
			array(
			  'name'  => 'White',
			  'slug'  => 'white',
			  'color'	=> '#ffffff',
			),
			array(
			  'name'  => 'Grey',
			  'slug'  => 'grey',
			  'color'	=> '#787878',
			),
			array(
			  'name'  => 'Blue',
			  'slug'  => 'blue',
			  'color'	=> '#3c4b55',
			),
			array(
			  'name'  => 'Light Blue',
			  'slug'  => 'light-blue',
			  'color'	=> '#95cae7',
			),
			array(
			  'name'  => 'Green',
			  'slug'  => 'green',
			  'color'	=> '#0b9500',
			)
		  ) );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'fsl' ),
				'menu-2' => esc_html__( 'Artists', 'fsl' ),
				'menu-3' => esc_html__( 'Footer', 'fsl' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'fsl_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'fsl_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function fsl_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'fsl_content_width', 640 );
}
add_action( 'after_setup_theme', 'fsl_content_width', 0 );

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );


/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Gutenberg scripts and styles
 */
function underscores_enqueue_gutenberg() {
	wp_register_style( 'underscores-gutenberg', get_stylesheet_directory_uri() . '/editor.css', array(), _S_VERSION, 'all' );
	wp_enqueue_style( 'underscores-gutenberg' );
	wp_enqueue_script( 'underscores-editor',  get_stylesheet_directory_uri() . '/js/build/editor-min.js', array( 'wp-blocks', 'wp-dom' ), _S_VERSION, true );
}
add_action( 'enqueue_block_editor_assets', 'underscores_enqueue_gutenberg' );

/**
 * Enqueue scripts and styles.
 */
function fsl_scripts() {
	wp_enqueue_style( 'fsl-style', get_stylesheet_uri(), array(), _S_VERSION );
	//wp_enqueue_style( 'fsl-style', get_stylesheet_uri(), array(), filemtime(get_stylesheet_directory() .'/style.css'), 'all' );
	wp_style_add_data( 'fsl-style', 'rtl', 'replace' );

	wp_register_script('app', get_template_directory_uri().'/js/build/site-min.js', array(), _S_VERSION, true );
	//wp_register_script('app', get_template_directory_uri().'/js/build/site-min.js', array(), filemtime(get_stylesheet_directory() .'/js/build/site-min.js'), true );
	wp_enqueue_script('app');
	
}
add_action( 'wp_enqueue_scripts', 'fsl_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 * ACF options
 */
if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page(array(
		'menu_title' => 'Site Settings',
		'page_title' => 'Site Settings',

	));
}

/**
 * Load ACF Blocks.
 */
if( function_exists('acf_register_block_type') ) {
	require get_template_directory() . '/inc/acf-blocks.php';
}

/**
 * Register Custom Post Types
 */
require_once( 'inc/custom-post-types/artist.php' );
require_once( 'inc/custom-post-types/product.php' );
require_once( 'inc/custom-post-types/order.php' );

/**
 * AJAX Review Comments
 */
add_action( 'wp_enqueue_scripts', 'aes_ajax_comments_scripts' );
function aes_ajax_comments_scripts() { 
	// just register for now, we will enqueue it below
	wp_register_script('ajax_comment', get_template_directory_uri().'/js/build/ajax-comment-min.js', array('jquery'), filemtime(get_stylesheet_directory() .'/js/build/ajax-comment-min.js'), true );
	// let's pass ajaxurl here, you can do it directly in JavaScript but sometimes it can cause problems, so better is PHP
	wp_localize_script( 'ajax_comment', 'aes_ajax_comment_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php'
	) );
 	wp_enqueue_script( 'ajax_comment' );
}

add_action( 'wp_ajax_ajaxcomments', 'aes_submit_ajax_comment' );
add_action( 'wp_ajax_nopriv_ajaxcomments', 'aes_submit_ajax_comment' ); 
function aes_submit_ajax_comment(){
	$comment = wp_handle_comment_submission( wp_unslash( $_POST ) );
	if ( is_wp_error( $comment ) ) {
		$error_data = intval( $comment->get_error_data() );
		if ( ! empty( $error_data ) ) {
			wp_die( '<p>' . $comment->get_error_message() . '</p>', __( 'Review submission failure', 'fsl' ), array( 'response' => $error_data, 'back_link' => true ) );
		} else {
			wp_die( 'Unknown error' );
		}
	}
	/* Set Cookies */
	$user = wp_get_current_user();
	do_action('set_comment_cookies', $comment, $user);
	/* Set the globals, so our comment functions below will work correctly */
	$comment_depth = 1;
	$GLOBALS['comment'] = $comment;
	$GLOBALS['comment_depth'] = $comment_depth;
	/* Comment HTML response  */
	$comment_html = '<div class="review-submitted-message" id="review-' . get_comment_ID() . '">';
	//if ( $comment->comment_approved == '0' ):
		$comment_html .= '<p class="review-awaiting-moderation">Thank you, your review is awaiting moderation.</p>';
		$comment_html .= '<button type="button" class="close-review-panel">Close</button>';
	//endif;
	$comment_html .= '</div>';
	echo $comment_html;
	/* End */
	die();
}

/* Additional Review Comment Fields */
add_action( 'comment_form_top', 'aes_additional_fields' );
function aes_additional_fields () {
	if ('product' == get_post_type()){
		echo '<div class="comment-form-rating"><p class="label">'. __('Rating', 'fsl') . '</p>';
		echo '<div class="review-rating-options">';
		//Current rating scale is 1 to 5. If you want the scale to be 1 to 10, then set the value of $i to 10.
		for( $i=1; $i <= 5; $i++ ){
			//echo '<span class="commentrating"><input type="radio" name="rating" id="review-rating" value="'. $i .'"/>'. $i .'</span>';
			if($i == 5):
				echo '<div class="review-rating-option"><input type="radio" id="rating-'. $i .'" name="rating" value="'. $i .'" checked /><label for="rating-'. $i .'">'. $i .'</label></div>';
			else:
				echo '<div class="review-rating-option"><input type="radio" id="rating-'. $i .'" name="rating" value="'. $i .'" /><label for="rating-'. $i .'">'. $i .'</label></div>';
			endif;
		}
		echo'</div></div>';
		echo '<p class="comment-form-review-summary">'.
		'<label for="title">' . __( 'Review summary', 'fsl' ) . '</label>'.
		'<input id="title" name="title" type="text" size="30" maxlength="245" /></p>';
	}
}

// Save the additional comment fields
add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {
	if ( ( isset( $_POST['title'] ) ) && ( $_POST['title'] != "") ) {
		$title = wp_filter_nohtml_kses($_POST['title']);
		add_comment_meta( $comment_id, 'title', $title );
	}

	if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != "") ) {
		$rating = wp_filter_nohtml_kses($_POST['rating']);
		add_comment_meta( $comment_id, 'rating', $rating );
	}
}

// Register meta box on comment edit screen
add_action( 'add_meta_boxes_comment', 'wpse248957_comment_edit_add_meta_box' );
function wpse248957_comment_edit_add_meta_box() {
    add_meta_box( 'title', __( 'Review', 'text-domain' ), 'wpse248957_comment_meta_box', 'comment', 'normal', 'high' );
}

// Callback function for displaying the comment meta box.
function wpse248957_comment_meta_box( $comment ) {
    $rating = get_comment_meta( $comment->comment_ID, 'rating', true );
    $review_summary = get_comment_meta( $comment->comment_ID, 'title', true );
    wp_nonce_field( 'wpse248957_comment_fields_update', 'wpse248957_comment_fields_update', false );
    ?>
	<p>
		<label for="title"><?php _e( 'Review summary', 'text-domain' ); ?></label>
		<input type="text" name="title" value="<?php echo esc_attr( $review_summary ); ?>" class="widefat" />
	</p>
    <p>
        <label for="rating"><?php _e( 'Rating', 'text-domain' ); ?></label>
        <input type="number" name="rating" value="<?php echo esc_attr( $rating ); ?>" class="widefat" />
    </p>
<?php
}

// Update comment meta data from comment editing screen 
add_action( 'edit_comment', 'wpse248957_comment_edit_meta_fields' );
function wpse248957_comment_edit_meta_fields( $comment_id ) {
    if ( ! isset( $_POST['wpse248957_comment_fields_update'] ) || 
             ! wp_verify_nonce( $_POST['wpse248957_comment_fields_update'], 'wpse248957_comment_fields_update' )
    ) {
        return;
    }

  if ( ( isset( $_POST['rating'] ) ) && ( $_POST['rating'] != '' ) ) {
        $rating = wp_filter_nohtml_kses( $_POST['rating'] );
        update_comment_meta( $comment_id, 'rating', $rating );
  } else {
        delete_comment_meta( $comment_id, 'rating');
  }
  if ( ( isset( $_POST['title'] ) ) && ( $_POST['title'] != '' ) ) {
        $title = wp_filter_nohtml_kses( $_POST['title'] );
        update_comment_meta( $comment_id, 'title', $title );
  } else {
        delete_comment_meta( $comment_id, 'title');
  }
}

// Add rating custom column to comments listing in admin
add_filter( 'manage_edit-comments_columns' , 'wpse248957_manage_comment_columns' );
function wpse248957_manage_comment_columns( $columns ) {
    return array_merge( $columns, 
        [ 
			'title' => __( 'Summary', 'text_domain' ),
			'rating' => __( 'Rating', 'text_domain' )
		]
    );
}

// Display rating custom column in comments listing in admin
add_action( 'manage_comments_custom_column' , 'wpse248957_manage_comments_columns', 10, 2 );
function wpse248957_manage_comments_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'title':
            $title = get_comment_meta( $post_id, 'title', true ) ?: '-';
            echo esc_html( $title );
            break;
		case 'rating':
			$rating = get_comment_meta( $post_id, 'rating', true ) ?: '-';
			echo esc_html( $rating );
			break;
    }
}


/* Use Comment Cookies */
function rudr_get_comment_author_data() {
	if( is_user_logged_in() ) {
		return false;
	}
	$comment_author = array();
	foreach( $_COOKIE as $cookie_key => $cookie_value ) { // loop through all the cookies
		if ( strpos( ' ' . $cookie_key, 'comment_author_email' ) > 0 ) { // is email matches
			$comment_author['email'] = urldecode( $cookie_value );
		} elseif ( strpos( ' ' . $cookie_key, 'comment_author' ) > 0 ) { // else if author name
			$comment_author['name'] = urldecode( $cookie_value );
		}
	}
	return $comment_author; // the result array
}



/* ACF Form */
add_filter('acf/pre_save_post' , 'my_pre_save_post', 1, 1 );
function my_pre_save_post( $post_id ) {
	// Create a new post
	$post = array(
		'post_status'  => 'publish' ,
		'post_type'  => 'order',
		'post_title'  => $_POST['acf']['field_6065a0a3e9883']
	);  
	// insert the post
	$post_id = wp_insert_post( $post );
	// return the new ID
	return $post_id;
}

/* Hex to rgb */
function hex2rgb( $colour ) {
	if ( $colour[0] == '#' ) {
			$colour = substr( $colour, 1 );
	}
	if ( strlen( $colour ) == 6 ) {
			list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
	} elseif ( strlen( $colour ) == 3 ) {
			list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
	} else {
			return false;
	}
	$r = hexdec( $r );
	$g = hexdec( $g );
	$b = hexdec( $b );
	//return array( 'red' => $r, 'green' => $g, 'blue' => $b );
	return $r . ', ' . $g . ', ' . $b;
}
