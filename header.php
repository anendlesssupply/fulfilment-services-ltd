<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fulfilment_Services_Ltd
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php locate_template("assets/svg/symbol-defs.svg", TRUE, TRUE); ?>
<?php wp_body_open(); ?>
<a class="skip-link screen-reader-text" href="#primary">
	<?php esc_html_e( 'Skip to content', 'fsl' ); ?>
</a>

<header id="masthead" class="site-header">
	<nav id="site-navigation" class="main-navigation">
		<button class="menu-toggle" aria-controls="primary-menu-wrapper" aria-expanded="false">
			<span class="screen-reader-text"><?php esc_html_e( 'Primary Menu', 'fsl' ); ?></span>
			<span class="site-logo-wrapper">
				<?php get_template_part( 'template-parts/site-logo' ); ?>
			</span>
		</button>
		<div id="primary-menu-wrapper" class="site-menu-wrapper">
			<div class="site-menu-wrapper-close">
				<button type="button" class="text-button" title="Close menu">
					<span class="screen-reader-text">Close menu</span>
					<svg class="icon icon-close" aria-hidden="true">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
					</svg>
				</button>

				<?php if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif; ?>
			</div>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'fallback_cb'	=> false,
					'container' => ''
				)
			);
			?>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-2',
					'menu_id'        => 'artists-menu',
					'fallback_cb'	=> false,
					'container' => ''
				)
			);
			?>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-3',
					'menu_id'        => 'footer-menu',
					'fallback_cb'	=> false,
					'container' => ''
				)
			);
			?>
		</div>
	</nav><!-- #site-navigation -->
	<div class="site-utilities">
		<?php if(is_page_template('templates/background-video.php') || is_page_template('templates/front.php')): ?>
			<button class="site-switch text-button" type="button" title="Switch background video">
				<span class="screen-reader-text">Switch background video</span>
				<svg class="icon switch-on" aria-hidden="true">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-toggle_on"></use>
				</svg>
				<svg class="icon switch-off" aria-hidden="true">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-toggle_off"></use>
				</svg>
			</button>
		<?php endif; ?>
		
		<?php $cart_page = get_field('cart_page', 'option');
		if($cart_page): ?>
			<button type="button" class="text-button site-cart-toggle">
				<span class="screen-reader-text"><?php echo get_the_title($cart_page); ?></span>
				<svg class="icon" aria-hidden="true">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cart"></use>
				</svg>
			</button>
		<?php endif; ?>
	</div>
</header><!-- #masthead -->

<div id="page" class="site">

	<?php $cart_page = get_field('cart_page', 'option');
	if($cart_page): ?>
		<div class="site-cart-panel">
			<div class="site-cart-contents">
				<h3><?php echo get_the_title($cart_page); ?></h3>
				<div class="site-cart-contents-header">
					<span>Item/s</span>
				</div>
				<div class="site-cart-contents-row">
					<span>&ndash;</span>
				</div>
			</div>
			<a class="button" href="<?php echo get_permalink($cart_page); ?>">
				Go to checkout
			</a>
		</div>
	<?php endif; ?>

	<div class="site-review-panel">
		<?php
		//while ( have_posts() ) :
			//the_post();
			if ( comments_open() ) :
				comments_template('/comments-review.php');
			else:
				echo '<h3>Reviews are closed</h3>';
			endif; 
		//endwhile;
		//wp_reset_postdata();
		?>
	</div>

	<?php get_template_part( 'template-parts/wall-of-reviews' ); ?>

	<div class="site-mask"></div>

	<?php do_action( 'aes_after_header_hook' );
