<?php
function my_register_blocks() {
  if( function_exists('acf_register_block_type') ) {
    
    acf_register_block_type(array(
      'name'              => 'box',
      'title'             => __('Box', 'fsl'),
      'description'       => __('A custom box block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'text',
      'mode'              => 'preview',
      'supports'          => array(
          'align' => true,
          'mode' => false,
          'jsx' => true
      ),
      'render_template' => 'template-parts/blocks/box/box.php',
    ));

    acf_register_block_type(array(
      'name'              => 'hero-text',
      'title'             => __('Hero Text', 'fsl'),
      'description'       => __('A custom hero text block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'text',
      'mode'              => 'preview',
      'supports'          => array(
          'align' => true,
          'mode' => false,
          'jsx' => true
      ),
      'render_template' => 'template-parts/blocks/hero-text/hero-text.php',
    ));

    acf_register_block_type(array(
      'name'              => 'toggle',
      'title'             => __('Toggle', 'fsl'),
      'description'       => __('A custom toggle block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'open-folder',
      'mode'              => 'preview',
      'supports'          => array(
          'align' => true,
          'mode' => false,
          'jsx' => true
      ),
      'render_template' => 'template-parts/blocks/toggle/toggle.php',
    ));

    acf_register_block_type(array(
      'name'              => 'marquee',
      'title'             => __('Marquee Text', 'fsl'),
      'description'       => __('A custom marquee text block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'text',
      'align'             => 'full',
      'mode' => 'auto',
      'supports'          => array(
        'align' => true,
      ),
      'keywords' => array('marquee', 'text', 'scrolling'),
      'render_template'   => 'template-parts/blocks/marquee/marquee.php',
    ));

    acf_register_block_type(array(
      'name'              => 'pip-video',
      'title'             => __('Picture in Picture Video', 'fsl'),
      'description'       => __('A custom video block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'embed-video',
      'mode'              => 'preview',
      'supports'          => array(
          'align' => true,
          'multiple' => false,
      ),
      'render_template' => 'template-parts/blocks/pip-video/pip-video.php',
    ));

    acf_register_block_type(array(
      'name'              => 'product-grid',
      'title'             => __('Product Grid', 'fsl'),
      'description'       => __('A custom product grid block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'grid-view',
      'mode' => 'auto',
      'supports'          => array(
          'align' => true,
      ),
      'render_template' => 'template-parts/blocks/product-grid/product-grid.php',
    ));

    acf_register_block_type(array(
      'name'              => 'text-circle',
      'title'             => __('Text Circle', 'fsl'),
      'description'       => __('A custom text circle block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'text',
      'mode' => 'auto',
      'supports'          => array(
        'align' => false,
      ),
      'keywords' => array('circle', 'text'),
      'render_template'   => 'template-parts/blocks/text-circle/text-circle.php',
    ));


  }
}
add_action('acf/init', 'my_register_blocks');

function add_block_editor_hero_text_assets(){
  wp_enqueue_style( 'block-hero-text', get_template_directory_uri() . '/template-parts/blocks/hero-text/build/hero-text.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/hero-text/build/hero-text.css') );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_hero_text_assets',10,0);

function add_block_editor_box_assets(){
  wp_enqueue_style( 'block-box', get_template_directory_uri() . '/template-parts/blocks/box/build/box.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/box/build/box.css') );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_box_assets',10,0);

function add_block_editor_marquee_assets(){
  wp_enqueue_style( 'block-marquee', get_template_directory_uri() . '/template-parts/blocks/marquee/build/marquee.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/marquee/build/marquee.css') );
  wp_enqueue_script( 'block-marquee', get_template_directory_uri() . '/template-parts/blocks/marquee/build/marquee-min.js', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/marquee/build/marquee-min.js'), true );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_marquee_assets',10,0);

function add_block_editor_toggle_assets(){
  wp_enqueue_style( 'block-toggle', get_template_directory_uri() . '/template-parts/blocks/toggle/build/toggle.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/toggle/build/toggle.css') );
  wp_enqueue_script( 'block-toggle', get_template_directory_uri() . '/template-parts/blocks/toggle/build/toggle-min.js', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/toggle/build/toggle-min.js'), true );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_toggle_assets',10,0);

function add_block_editor_pip_video_assets(){
  wp_enqueue_style( 'block-pip-video', get_template_directory_uri() . '/template-parts/blocks/pip-video/build/pip-video.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/pip-video/build/pip-video.css') );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_pip_video_assets',10,0);

function add_block_editor_product_grid_assets(){
  wp_enqueue_style( 'block-product-grid', get_template_directory_uri() . '/template-parts/blocks/product-grid/build/product-grid.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/product-grid/build/product-grid.css') );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_product_grid_assets',10,0);

function add_block_editor_text_circle_assets(){
  wp_enqueue_style( 'block-text-circle', get_template_directory_uri() . '/template-parts/blocks/text-circle/build/text-circle.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/text-circle/build/text-circle.css') );
  wp_enqueue_script( 'block-text-circle', get_template_directory_uri() . '/template-parts/blocks/text-circle/build/text-circle-min.js', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/text-circle/build/text-circle-min.js'), true );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_text_circle_assets',10,0);
