<?php
add_action( 'init', 'my_artist_cpt' );
function my_artist_cpt() {
  $labels = array(
    'name'               => _x( 'Artists', 'post type general name', 'fsl' ),
    'singular_name'      => _x( 'Artist', 'post type singular name', 'fsl' ),
    'menu_name'          => _x( 'Artists', 'admin menu', 'fsl' ),
    'name_admin_bar'     => _x( 'Artists', 'add new on admin bar', 'fsl' ),
    'add_new'            => _x( 'Add New', 'Artist', 'fsl' ),
    'add_new_item'       => __( 'Add New Artist', 'fsl' ),
    'new_item'           => __( 'New Artist', 'fsl' ),
    'edit_item'          => __( 'Edit Artist', 'fsl' ),
    'view_item'          => __( 'View Artist', 'fsl' ),
    'all_items'          => __( 'All Artists', 'fsl' ),
    'search_items'       => __( 'Search Artists', 'fsl' ),
    'parent_item_colon'  => __( 'Parent Artist:', 'fsl' ),
    'not_found'          => __( 'No Artists found.', 'fsl' ),
    'not_found_in_trash' => __( 'No Artists found in Trash.', 'fsl' )
  );
 
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Artists', 'fsl' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'with_front' => false, 'slug' => 'artists' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-art',
    'show_in_rest'       => true,
    'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail' )
  );
 
  register_post_type( 'artist', $args );
}
