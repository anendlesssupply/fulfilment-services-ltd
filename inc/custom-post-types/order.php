<?php
add_action( 'init', 'my_order_cpt' );
function my_order_cpt() {
  $labels = array(
    'name'               => _x( 'Orders', 'post type general name', 'fsl' ),
    'singular_name'      => _x( 'Order', 'post type singular name', 'fsl' ),
    'menu_name'          => _x( 'Orders', 'admin menu', 'fsl' ),
    'name_admin_bar'     => _x( 'Orders', 'add new on admin bar', 'fsl' ),
    'add_new'            => _x( 'Add New', 'Order', 'fsl' ),
    'add_new_item'       => __( 'Add New Order', 'fsl' ),
    'new_item'           => __( 'New Order', 'fsl' ),
    'edit_item'          => __( 'Edit Order', 'fsl' ),
    'view_item'          => __( 'View Order', 'fsl' ),
    'all_items'          => __( 'All Orders', 'fsl' ),
    'search_items'       => __( 'Search Orders', 'fsl' ),
    'parent_item_colon'  => __( 'Parent Order:', 'fsl' ),
    'not_found'          => __( 'No Orders found.', 'fsl' ),
    'not_found_in_trash' => __( 'No Orders found in Trash.', 'fsl' )
  );
 
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Orders', 'fsl' ),
    'public'             => false,
    'publicly_queryable' => false,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'with_front' => false, 'slug' => 'orders' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-art',
    'show_in_rest'       => true,
    'supports'           => array( 'title' )
  );
 
  register_post_type( 'order', $args );
}
