<?php
add_action( 'init', 'my_product_cpt' );
function my_product_cpt() {
  $labels = array(
    'name'               => _x( 'Products', 'post type general name', 'fsl' ),
    'singular_name'      => _x( 'Product', 'post type singular name', 'fsl' ),
    'menu_name'          => _x( 'Products', 'admin menu', 'fsl' ),
    'name_admin_bar'     => _x( 'Products', 'add new on admin bar', 'fsl' ),
    'add_new'            => _x( 'Add New', 'Product', 'fsl' ),
    'add_new_item'       => __( 'Add New Product', 'fsl' ),
    'new_item'           => __( 'New Product', 'fsl' ),
    'edit_item'          => __( 'Edit Product', 'fsl' ),
    'view_item'          => __( 'View Product', 'fsl' ),
    'all_items'          => __( 'All Products', 'fsl' ),
    'search_items'       => __( 'Search Products', 'fsl' ),
    'parent_item_colon'  => __( 'Parent Product:', 'fsl' ),
    'not_found'          => __( 'No Products found.', 'fsl' ),
    'not_found_in_trash' => __( 'No Products found in Trash.', 'fsl' )
  );
 
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Products', 'fsl' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'with_front' => false, 'slug' => 'products' ),
    'capability_type'    => 'post',
    'has_archive'        => false,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-art',
    'show_in_rest'       => true,
    'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments' )
  );
 
  register_post_type( 'product', $args );
}
