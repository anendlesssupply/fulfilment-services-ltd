(function () {
	'use strict';

	/* eslint-disable no-undef */
	jQuery.extend( jQuery.fn, {
		/*
		 * check if field value lenth more than 3 symbols ( for name and comment )
		 */
		validate() {
			if ( jQuery( this ).val().length < 3 ) {
				jQuery( this ).addClass( 'error' ); return false;
			}
			jQuery( this ).removeClass( 'error' ); return true;
		},
		/*
		 * check if email is correct
		 * add to your CSS the styles of .error field, for example border-color:red;
		 */
		validateEmail() {
			const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
				emailToValidate = jQuery( this ).val();
			if ( ! emailReg.test( emailToValidate ) || emailToValidate == '' ) {
				jQuery( this ).addClass( 'error' ); return false;
			}
			jQuery( this ).removeClass( 'error' ); return true;
		},
		validateCheckbox() {
			if ( ! jQuery( this ).checked ) {
				jQuery( this ).addClass( 'error' ); return false;
			}
			jQuery( this ).removeClass( 'error' ); return true;
		},
	} );

	jQuery( function( $ ) {
	/*
	* On comment form submit
	*/
		$( '#review-form' ).submit( function() {
			// define some vars
			const button = $( '#review-submit' ); // submit button
			const respond = $( '.review-comment-respond' ); // comment form container

			// if user is logged in, do not validate author and email fields
			const author = respond.find( '#review-author' );
			if ( author && author.length ) {
				author.validate();
			}

			const email = respond.find( '#review-email' );
			if ( email && email.length ) {
				email.validateEmail();
			}

			// validate comment in any case
			const comment = respond.find( '#review-comment' );

			if ( comment ) {
				comment.validate();
			}

			const title = respond.find( '#title' );
			if ( title && title.length ) {
				title.validate();
			}
			const cookieConsent = respond.find( '#review-wp-comment-cookies-consent' );
			if ( cookieConsent && cookieConsent.length ) {
				cookieConsent.validateCheckbox();
			}

			// if comment form isn't in process, submit it
			if ( ! button.hasClass( 'loadingform' ) && ! author.hasClass( 'error' ) && ! email.hasClass( 'error' ) && ! comment.hasClass( 'error' ) ) {
				// ajax request
				$.ajax( {
					type: 'POST',
					url: aes_ajax_comment_params.ajaxurl, // admin-ajax.php URL
					data: $( this ).serialize() + '&action=ajaxcomments', // send form data + action parameter
					beforeSend( xhr ) {
						// what to do just after the form has been submitted
						button.addClass( 'loadingform' ).val( 'Loading...' );
					},
					error( request, status, error ) {
						if ( status === 500 ) {
							alert( 'Error while adding comment' );
						} else if ( status == 'timeout' ) {
							alert( 'Error: Server doesn\'t respond.' );
						} else {
							// process WordPress errors
							const wpErrorHtml = request.responseText.split( '<p>' ),
								wpErrorStr = wpErrorHtml[ 1 ].split( '</p>' );

							alert( wpErrorStr[ 0 ] );
						}
					},
					success( addedCommentHTML ) {
						// remove form
						respond.empty().before( addedCommentHTML );
						$( '.close-review-panel' ).on( 'click', function( e ) {
							if ( document.body.classList.contains( 'site-review-panel-is-active' ) ) {
								document.body.classList.remove( 'site-review-panel-is-active' );
								document.body.classList.remove( 'site-cart-panel-is-active' );
								$( 'body' ).off( 'keydown' );
							}
						} );

						// update localstorage
						const currentProductId = $( '#product-data-id' ).text();
						const currentProductTitle = $( '#product-data-title' ).text();
						const currentProduct = {
							id: currentProductId,
							title: currentProductTitle,
						};
						//console.log( { currentProduct } );
						const storedSubmittedProductReviews = JSON.parse( localStorage.getItem( 'submittedProductReviews' ) ) || [];
						//console.log( { storedSubmittedProductReviews } );
						const currentProductExists = storedSubmittedProductReviews.filter( ( item ) => item.id === currentProductId ) || null;
						//console.log( { currentProductExists } );
						if ( ! storedSubmittedProductReviews.length > 0 || ! currentProductExists.length > 0 ) {
							const submittedProductReviews = [ ...storedSubmittedProductReviews, currentProduct ];
							localStorage.setItem( 'submittedProductReviews', JSON.stringify( submittedProductReviews ) );
							//console.log( { submittedProductReviews } );
							// remove disabled flag
							const addToCartButton = document.querySelector( '.add-to-cart-wrapper button' ) || null;
							addToCartButton.disabled = false;
						}
					},
					complete() {
						// what to do after a comment has been added
						// button.removeClass( 'loadingform' ).val( 'Submit review' );
					},
				} );
			}
			return false;
		} );
	} );

}());
