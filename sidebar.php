<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Fulfilment_Services_Ltd
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}