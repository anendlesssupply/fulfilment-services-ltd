<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Fulfilment_Services_Ltd
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			get_template_part( 'template-parts/post-navigation' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();
