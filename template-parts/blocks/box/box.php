<?php
$id = 'wp-block-acf-box-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-box';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$has_text_shadow = get_field('has_text_shadow');
if($has_text_shadow):
    $className .= ' has-text-shadow';
endif;

$style = "";
$text_colour = get_field('text_colour');
if($text_colour):
    $style .= ' --colorText: ' . $text_colour . ';';
endif;
$link_colour = get_field('link_colour');
if($link_colour):
    $style .= ' --colorLink: ' . $link_colour . ';';
endif;
$link_hover_colour = get_field('link_hover_colour');
if($link_hover_colour):
    $style .= ' --colorLinkHover: ' . $link_hover_colour . ';';
endif;
$background_colour = get_field('background_colour');
if($background_colour):
    $rgb = hex2rgb($background_colour);
    $style .= ' --blockBackground: rgba( ' . $rgb . ', 90%);';
    $className .= ' has-block-background';
endif;

$columns = get_field('columns') ?: 1; 

$template = array(
    array( 'core/heading', array(
        'placeholder' => 'Add a heading',
    ) ),
);

?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" style="<?php echo $style; ?>">
    <InnerBlocks template="<?php esc_attr( wp_json_encode( $template ) ); ?>" />
</div>