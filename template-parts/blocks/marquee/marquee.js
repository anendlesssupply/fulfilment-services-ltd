import debounce from 'lodash.debounce';

export function initMarqueeBlock( el ) {
	if ( ! el ) {
		return;
	}

	const marqueeContainer = el.querySelector( '.marquee' ) || null;
	const marqueeInner = el.querySelector( '.marquee__inner' ) || null;
	const marqueeItem = el.querySelector( '.marquee__item' ) || null;
	if ( marqueeContainer && marqueeInner && marqueeItem ) {
		//deactivate and remove clones
		marqueeContainer.classList.remove( 'marquee-init' );
		const clonedInnersToRemove = marqueeContainer.querySelectorAll( '.marquee__inner--clone' ) || null;
		if ( clonedInnersToRemove && clonedInnersToRemove.length > 0 ) {
			clonedInnersToRemove.forEach( ( clone ) => clone.remove() );
		}
		const clonedItemsToRemove = marqueeContainer.querySelectorAll( '.marquee__item--clone' ) || null;
		if ( clonedItemsToRemove && clonedItemsToRemove.length > 0 ) {
			clonedItemsToRemove.forEach( ( clone ) => clone.remove() );
		}

		// calculate items to fill screen width
		const containerWidth = marqueeContainer.offsetWidth;
		const itemWidth = marqueeItem.offsetWidth;
		const numberOfItems = Math.ceil( containerWidth / itemWidth );

		// clone items
		let cloneCounter;
		let clonedItem;
		for ( cloneCounter = 1; cloneCounter <= numberOfItems; ++cloneCounter ) {
			clonedItem = marqueeItem.cloneNode( true );
			clonedItem.classList.add( 'marquee__item--clone' );
			marqueeInner.append( clonedItem );
		}

		// clone inner
		const clonedMarqueeInner = marqueeInner.cloneNode( true );
		clonedMarqueeInner.classList.add( 'marquee__inner--clone' );
		marqueeContainer.append( clonedMarqueeInner );

		// set duration
		const marqueeSpeed = containerWidth * .0125;
		//const marqueeSpeed = window.innerWidth * .0125;
		const marqueeDuration = ( ( parseInt( itemWidth, 10 ) + parseInt( containerWidth, 10 ) ) / parseInt( containerWidth, 10 ) ) * marqueeSpeed;
		//const marqueeDuration = (parseInt(itemWidth, 10)  / parseInt(containerWidth, 10)) * marqueeSpeed;
		marqueeContainer.style.setProperty( '--marqueeDuration', `${ marqueeDuration }s` );

		//activate
		marqueeContainer.classList.add( 'marquee-init' );
	}
}

export const debounceUpdateMarquees = debounce( ( el ) => {
	if ( el && el.length > 0 ) {
		el.forEach( ( marquee ) => {
			initMarqueeBlock( marquee );
		} );
	}
}, 1000, { trailing: true } );

const initPreviewMarqueeBlock = ( el ) => {
	const block = el[ 0 ].querySelector( '.wp-block-acf-marquee' ) || null;
	if ( block ) {
		initMarqueeBlock( block );
	}
};

// Initialize dynamic block preview (editor).
if ( window.acf ) {
	window.acf.addAction(
		'render_block_preview/type=marquee',
		initPreviewMarqueeBlock
	);
}
