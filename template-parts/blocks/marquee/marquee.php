<?php
$id = 'wp-block-acf-marquee-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-marquee';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$has_text_shadow = get_field('has_text_shadow');
if($has_text_shadow):
    $className .= ' has-text-shadow';
endif;

$transparent_background = get_field('transparent_background');
if($transparent_background):
    $className .= ' has-no-background';
endif;

$font_size = get_field('font_size');
if($font_size):
    $className .= ' font-size-' . $font_size;
endif;

$text = get_field('text') ?: '';
$link = get_field('link') ?: '';
$autoplay = get_field('autoplay') ?: false;
$fixed = get_field('fixed') ?: false;
if(!$autoplay){
    $className .= ' wp-block-acf-marquee--no-autoplay';
}
if($fixed){
    $className .= ' wp-block-acf-marquee--is-fixed';
}

$style = "";

$text_colour = get_field('text_colour');
if($text_colour):
    $style .= ' --colorText: ' . $text_colour . ';';
endif;
$link_colour = get_field('link_colour');
if($link_colour):
    $style .= ' --colorLink: ' . $link_colour . ';';
endif;
$link_hover_colour = get_field('link_hover_colour');
if($link_hover_colour):
    $style .= ' --colorLinkHover: ' . $link_hover_colour . ';';
endif;
$background_colour = get_field('background_colour');
if($background_colour):
    $rgb = hex2rgb($background_colour);
    $style .= ' --blockBackground: rgba( ' . $rgb . ', 90%);';
    //$style .= ' --blockBackground: ' . $background_colour . ';';
    $className .= ' has-block-background';
endif;


if($text): ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" role="marquee"  style="<?php echo $style; ?>">
        <span class="screen-reader-text"><?php echo $text; ?></span>
        <div class="marquee">
            <div class="marquee__inner" aria-hidden="true">
                <?php if($link): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                    <span class="marquee__item"><a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php esc_html( $link_title ); ?>"><?php echo $text; ?></a></span>
                <?php else: ?>
                    <span class="marquee__item"><?php echo $text; ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php else:
    if( $is_preview === true ): ?>
        <p class="wp-block-acf-is-empty">Please enter the marquee text.</p>
    <?php endif;
endif;