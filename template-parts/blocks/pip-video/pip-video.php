<?php
$id = 'wp-block-acf-pip-video-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-pip-video';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$background_video = get_field('background_video');
$background_image = get_field('background_image');

if($background_video): 
    $background_video_type = $background_video['type'] ?: ''; 
    $background_video_subtype = $background_video['subtype'] ?: '';
    if($background_video_type && $background_video_subtype):
        $background_video_type = $background_video_type . '/' . $background_video_subtype;
    endif; ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <video <?php if($background_image): echo 'poster="' . $background_image . '"'; endif; ?> playsinline autoplay muted loop>
            <source src="<?php echo $background_video['url']; ?>" type="<?php echo $background_video_type; ?>">
        </video>
    </div>
<?php else:
    if( $is_preview === true ): ?>
        <p class="wp-block-acf-is-empty">Please add a video file.</p>
    <?php endif;
endif;