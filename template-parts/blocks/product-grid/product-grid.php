<?php
$id = 'wp-block-acf-product-grid-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-product-grid';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$products = get_field('products') ?: "";
if($products): ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <?php foreach($products as $product_id): ?>
            <div class="product-item">
                <a href="<?php echo get_the_permalink($product_id); ?>">
                    <?php 
                    $image_id = get_post_thumbnail_id($product_id);
                    $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
                    if( $image_id ):
                        echo wp_get_attachment_image( $image_id, $size );
                    endif; 
                    $artist_name = get_field('artist_name', $product_id);
                    $product_title = get_field('product_title', $product_id);
                    $post_title = get_the_title($product_id);
                    if(!$artist_name && !$product_title): ?>
                        <h2><?php echo $post_title; ?></h2>
                    <?php else: ?>
                        <h2>
                            <?php if($artist_name): echo $artist_name; endif;
                            if($artist_name && $product_title): echo '<br />'; endif;
                            if($product_title): echo '<span>' . $product_title . '</span>'; endif; ?>
                        </h2>
                    <?php endif; 
                    $stock_message = get_field('stock_message', $product_id); 
                    if($stock_message): ?>
                        <p class="product-item__stock"><?php echo $stock_message; ?></p>
                    <?php endif; ?>
                    <p>Leave a review and get yours now!</p>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: 
    if( $is_preview === true ): ?>
        <p class="wp-block-acf-is-empty">Please select some products.</p>
    <?php endif;
endif;