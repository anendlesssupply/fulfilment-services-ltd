import charming from 'charming';

export function initTextCircleBlock( el ) {
	if ( ! el ) {
		return;
	}
	const handleLettering = ( text ) => {
		charming( text );
	};

	const content = el.querySelector( '.text-circle-content' ) || null;
	if ( content ) {
		handleLettering( content );
	}
}

const initPreviewTextCircleBlock = ( el ) => {
	const block = el[ 0 ].querySelector( '.wp-block-acf-text-circle' ) || null;
	if ( block ) {
		initTextCircleBlock( block );
	}
};

// Initialize dynamic block preview (editor).
if ( window.acf ) {
	window.acf.addAction(
		'render_block_preview/type=text-circle',
		initPreviewTextCircleBlock
	);
}
