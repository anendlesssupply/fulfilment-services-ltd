<?php
$id = 'wp-block-acf-text-circle-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-text-circle';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$text = get_field('text') ?: '';
$link = get_field('link') ?: '';
$image_id = get_field('image') ?: '';
$rotation = get_field('rotation') ?: '90';
$align = get_field('align') ?: 'left';
$fixed = get_field('fixed');
$x_value = get_field('x_coordinate') ?: 100;
$y_value = get_field('y_coordinate') ?: 100;
if( !empty( $align ) ) {
    $className .= ' wp-block-acf-text-circle--align-' . $align;
}
if( !empty( $fixed ) ) {
    $className .= ' wp-block-acf-text-circle--is-fixed';
}
if($text): ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>" style="--rotation:<?php echo $rotation . 'deg'; ?>; --xValue:<?php echo $x_value . '%'; ?>; --yValue:<?php echo $y_value . 'vh'; ?>;">
        <div class="text-circle-content-wrapper">
            <?php if($link): 
                $link_url = $link['url'];
                $link_title = $link['title'] ?: $text;
                $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                <a class="text-circle-content" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php esc_html( $link_title ); ?>"><?php echo $text; ?></a>
                <?php if($image_id): 
                    $size = 'thumbnail'; ?>
                    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php esc_html( $link_title ); ?>">
                        <?php echo wp_get_attachment_image( $image_id, $size ); ?>
                    </a>
                <?php endif; ?>
            <?php else: ?>
                <p class="text-circle-content"><?php echo $text; ?></p>
                <?php if($image_id): 
                    $size = 'thumbnail';
                    echo wp_get_attachment_image( $image_id, $size );
                endif; ?>
            <?php endif; ?>
        </div>
    </div>
<?php else: 
    if( $is_preview === true ): ?>
        <p class="wp-block-acf-is-empty">Please add some text.</p>
    <?php endif;
endif;