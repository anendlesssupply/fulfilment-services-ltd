<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

?>

<canvas id="canvas" width="100%" height="100%" aria-hidden="true"></canvas>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'fsl' ); ?></h1>
	</header><!-- .page-header -->
</section><!-- .no-results -->
