<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header screen-reader-text">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

    <?php $background_video = get_field('background_video');
    $background_image = get_field('background_image'); 
    $mask = get_field('mask'); 
    $mask_colour = get_field('mask_colour') ?: '#000'; 
    $mask_opacity = get_field('mask_opacity') ?: '0.5';
    if($background_video): 
        $background_video_type = $background_video['type'] ?: ''; 
        $background_video_subtype = $background_video['subtype'] ?: '';
        if($background_video_type && $background_video_subtype):
            $background_video_type = $background_video_type . '/' . $background_video_subtype;
        endif; ?>
        <div class="site-bg-vid-wrapper <?php if($mask): echo 'site-bg-vid-wrapper--has-mask'; endif; ?>" style="--maskBackground:<?php echo $mask_colour; ?>;--maskOpacity:<?php echo $mask_opacity; ?>;">
            <video class="site-bg-vid" <?php if($background_image): echo 'poster="' . $background_image . '"'; endif; ?> playsinline autoplay muted loop>
                <source src="<?php echo $background_video['url']; ?>" type="<?php echo $background_video_type; ?>">
            </video>
        </div>
        
        <video class="site-pip" <?php if($background_image): echo 'poster="' . $background_image . '"'; endif; ?> playsinline autoplay muted loop>
            <source src="<?php echo $background_video['url']; ?>" type="<?php echo $background_video_type; ?>">
        </video>
    <?php endif; ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'fsl' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'fsl' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
