<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

?>

<canvas id="canvas" width="100%" height="100%" aria-hidden="true"></canvas>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header screen-reader-text">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		$orders_open = get_field('orders_open', 'option');
		if($orders_open): ?>
			<div class="checkout-wrapper">
				<div class="checkout-column checkout-column--cart">
					<h3><?php echo get_field('your_cart_label', 'option') ?: 'Your Cart'; ?></h3>
				</div>
				<div class="checkout-column">
					<h3><?php echo get_field('shipping_details_label', 'option') ?: 'Shipping Details'; ?></h3>
					<?php 
					$order_received_page_id = get_field('order_received_page', 'option');
					$return_url = $order_received_page_id ? get_the_permalink($order_received_page_id) : home_url( '/' );
					$settings = array(
						'id' => 'acf-form',
						'post_id' => 'new_post',
						'honeypot' => true,
						'fields' => array(
							'name',
							'address',
							'city',
							'country',
							'postcode',
							'email',
							'cart_contents',
						),
						'return' => $return_url,
						'submit_value'  => 'Place order',
						'updated_message' => __("Post updated", 'acf'),
						//'uploader' => 'basic',
					);
					acf_form($settings); ?>
				</div>
			</div>
		<?php else: ?>
			<div class="checkout-wrapper">
				<div class="checkout-column">
					<h3><?php echo get_field('orders_are_closed_label', 'option') ?: 'Orders are Closed'; ?></h3>
				</div>
			</div>
        <?php endif; ?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'fsl' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
