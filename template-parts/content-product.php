<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

?>
<canvas id="canvas" width="100%" height="100%" aria-hidden="true"></canvas>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <span id="product-data-id"><?php the_ID(); ?></span>
    <span id="product-data-title"><?php the_title(); ?></span>
    <div class="product-wrapper">
        <div class="product-info">
            <header class="product-header">
                <div class="product-header-row">
                    <?php 
                    $product_id = get_the_ID();
                    $artist_name = get_field('artist_name', $product_id);
                    $product_title = get_field('product_title', $product_id);
                    $post_title = get_the_title($product_id);
                    if(!$artist_name && !$product_title): ?>
                        <h1 class="product-title"><?php echo $post_title; ?></h1>
                    <?php else: ?>
                        <h1 class="product-title">
                            <?php if($artist_name): echo $artist_name; endif;
                            if($artist_name && $product_title): echo '<br />'; endif;
                            if($product_title): echo $product_title; endif; ?>
                        </h1>
                    <?php endif; ?>
                    <?php
                    $stock_message = get_field('stock_message', $product_id); 
                    if($stock_message): ?>
                        <p class="product-stock"><?php echo $stock_message; ?></p>
                    <?php endif; ?>
                </div>
                <div class="product-header-row">
                    <div class="product-header-column">
                        <?php
                        $sku = get_field('sku', $product_id); 
                        if($sku): ?>
                            <p class="product-sku"><?php echo $sku; ?></p>
                        <?php endif; 
                        $product_short_desc = get_field('product_short_description', $product_id); 
                        if($product_short_desc): ?>
                            <p class="product-delivery-desc"><?php echo $product_short_desc; ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="add-to-cart-wrapper">
                        <button data-product-id="<?php echo get_the_ID(); ?>" data-product-title="<?php echo get_the_title(); ?>" type="button" disabled>Add to cart</button>
                        <span><span class="em-icon">!</span> <?php echo get_field('leave_a_review_label', 'option') ?: 'Leave a review'; ?></span>
                    </div>
                </div>
            </header><!-- .entry-header -->
            <div class="product-info__section">
                <h3>Reviews</h3>
                <div class="product-info-row">
                    <?php
                    $star_rating = get_field('star_rating', $product_id);
                    ?>
                    <div class="product-info-column">
                        <div class="product-stars" data-rating="<?php if($star_rating): echo $star_rating; else: echo '0'; endif; ?>">
                            <?php get_template_part( 'template-parts/rating-stars' ); ?>
                            <p class="product-stars__text">
                                <?php
                                if($star_rating == '0'):
                                    echo '[No votes]';
                                    //echo get_field('ratings_label_0_stars', 'option') ?: 'Bad';
                                elseif($star_rating == '1'):
                                    echo get_field('ratings_label_1_stars', 'option') ?: 'Poor';
                                elseif($star_rating == '2'):
                                    echo get_field('ratings_label_2_stars', 'option') ?: 'Ok';
                                elseif($star_rating == '3'):
                                    echo get_field('ratings_label_3_stars', 'option') ?: 'Good';
                                elseif($star_rating == '4'):
                                    echo get_field('ratings_label_4_stars', 'option') ?: 'Very Good';
                                elseif($star_rating == '5'):
                                    echo get_field('ratings_label_5_stars', 'option') ?: 'Great';
                                elseif(!$star_rating):
                                    echo '[No votes]';
                                endif; ?>    
                            </p>
                        </div>
                    </div>

                    <div class="add-a-review-wrapper">
                        <button class="site-review-toggle" type="button"><?php echo get_field('write_a_review_label', 'option') ?: 'Write a review'; ?></button>
                    </div>
                </div>
            </div>
            
            <?php 
            $args = array(
                'post_id' => get_the_ID(),
                'status' => 'approve'
            );
            $comments = get_comments( $args ); 
            $total_comments = 0;
            //$comments = get_approved_comments(get_the_ID(), $args); 
            if($comments):
                $total_comments = count($comments); ?>
                <section class="product-info__section product-info__section--text">
                    <header>
                        <button class="text-button" type="button"><?php echo get_field('customer_reviews_label', 'option') ?: 'Customer reviews'; ?> (<?php echo $total_comments; ?>) <span aria-hidden="true" class="expand-icon">+</span><span aria-hidden="true" class="hide-icon">-</span></button>
                    </header>
                    <div class="product-info__section-content">
                        <?php foreach($comments as $comment): ?>
                            <?php 
                            $comment_id = $comment->comment_ID;
                            $comment_author = $comment->comment_author;
                            $comment_content = $comment->comment_content;
                            $comment_summary = get_comment_meta( $comment_id, 'title', true );
                            $comment_rating = get_comment_meta( $comment_id, 'rating', true);
                            ?>
                            <div class="product-customer-review">
                                <header class="product-customer-review__header">
                                    <div class="product-stars" data-rating="<?php if($comment_rating): echo $comment_rating; else: echo '0'; endif; ?>">
                                        <?php get_template_part( 'template-parts/rating-stars' ); ?>
                                        <p class="product-stars__text">
                                            <?php echo $comment_rating . '/5'; ?>
                                        </p>
                                    </div>
                                    <?php if($comment_summary): ?>
                                        <h4><?php echo $comment_summary; ?></h4>
                                    <?php endif; ?>
                                </header>
                                <div class="product-customer-review__content">
                                    <p class="product-customer-review__content-author">
                                        <?php echo get_field('anonymous_author_label', 'option') ?: 'Anonymous'; ?>
                                    </p>
                                    <div class="product-customer-review__content-main"><p><?php if($comment_content): echo "&ldquo;" . $comment_content . "&rdquo;"; endif; ?></p></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </section>
            <?php endif; ?>

            <?php 
            if( have_rows('info_sections') ):
                while( have_rows('info_sections') ) : the_row();
                    $header = get_sub_field('header');
                    $content = get_sub_field('content');
                    if($header && $content): ?>
                        <section class="product-info__section product-info__section--text">
                            <header>
                                <button class="text-button" type="button"><?php echo $header; ?> <span aria-hidden="true" class="expand-icon">+</span><span aria-hidden="true" class="hide-icon">-</span></button>
                            </header>
                            <div class="product-info__section-content">
                                <?php echo $content; ?>
                            </div>
                        </section>
                    <?php endif;
                endwhile;
            endif; ?>
        </div>
            
        <div class="product-content entry-content">
            <?php
            the_content();

            wp_link_pages(
                array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'fsl' ),
                    'after'  => '</div>',
                )
            );
            ?>
        </div><!-- .entry-content -->
    </div>

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'fsl' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
