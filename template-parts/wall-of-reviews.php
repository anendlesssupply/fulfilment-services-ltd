<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

?>

<?php 
$page_id = get_field('wall_of_reviews_page', 'option');
if(!$page_id):
    return;
endif;
?>
<div class="wall-of-reviews-wrapper">
    <?php $background_video = get_field('background_video', $page_id);
    $background_image = get_field('background_image', $page_id); 
    $mask = get_field('mask', $page_id); 
    $mask_colour = get_field('mask_colour', $page_id) ?: '#000'; 
    $mask_opacity = get_field('mask_opacity', $page_id) ?: '0.5';
    if($background_video): 
        $background_video_type = $background_video['type'] ?: ''; 
        $background_video_subtype = $background_video['subtype'] ?: '';
        if($background_video_type && $background_video_subtype):
            $background_video_type = $background_video_type . '/' . $background_video_subtype;
        endif;
        ?>
        <div class="site-bg-vid-wrapper site-bg-vid-wrapper--wall-of-reviews <?php if($mask): echo 'site-bg-vid-wrapper--has-mask'; endif; ?>" style="--maskBackground:<?php echo $mask_colour; ?>;--maskOpacity:<?php echo $mask_opacity; ?>;">
            <video class="site-bg-vid" <?php if($background_image): echo 'poster="' . $background_image . '"'; endif; ?> playsinline autoplay muted loop>
                <source data-src="<?php echo $background_video['url']; ?>" type="<?php echo $background_video_type; ?>">
            </video>
        </div>
    <?php endif; ?>

    <?php 
    $args = array(
        'status' => 'approve'
    );
    $comments = get_comments( $args );
    if($comments):
        shuffle($comments); ?>
        <div class="wall-of-reviews">
            <?php foreach($comments as $comment): ?>
                <?php 
                $comment_id = $comment->comment_ID;
                $comment_author = $comment->comment_author;
                $comment_content = $comment->comment_content;
                $comment_summary = get_comment_meta( $comment_id, 'title', true );
                $comment_rating = get_comment_meta( $comment_id, 'rating', true);
                ?>
                <div class="product-customer-review">
                    <header class="product-customer-review__header">
                        <div class="product-stars" data-rating="<?php if($comment_rating): echo $comment_rating; else: echo '0'; endif; ?>">
                            <?php get_template_part( 'template-parts/rating-stars' ); ?>
                            <p class="product-stars__text">
                                <?php echo $comment_rating . '/5'; ?>
                            </p>
                        </div>
                        <?php if($comment_summary): ?>
                            <h4><?php echo $comment_summary; ?></h4>
                        <?php endif; ?>
                    </header>
                    <div class="product-customer-review__content">
                        <p class="product-customer-review__content-author">
                            <?php echo get_field('anonymous_author_label', 'option') ?: 'Anonymous'; ?>
                        </p>
                        <div class="product-customer-review__content-main"><p><?php if($comment_content): echo "&ldquo;" . $comment_content . "&rdquo;"; endif; ?></p></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <button type="button" class="destroy-wall"><span><?php echo get_field('im_still_here_label', 'option') ?: 'I&rsquo;m still here'; ?></span></button>
</div>